const functions = require('firebase-functions')
const admin = require('firebase-admin')

// const stripe = require('stripe')
// let stripePrivateKey = functions.config().stripe.private_key
// let stripePublicKey = functions.config().stripe.public_key
// const stripeApp = stripe(stripePrivateKey)

// activate cors authorization for querying from frontends
const cors = require('cors')({
	origin: true
})


const publishJob = (req, res) => {
	// the price to publish a job
	const amount = 1400

	// use cors to make the answer work
	// between the client and the server (not served by same URL)
	return cors(req, res, () => {
		// get the content of the request sent by the client
		const {body} = req;
		if (!body) {
			res.send({
				data: {
					error: 'You need to provide an object with a job id:string and a card:object'
				}
			});
		}
		// get the data passed by the front end
		const { id, token, coupon } = body.data
		console.log('body', body)
		console.log('id', id)
		console.log('token', token)
		console.log('coupon', coupon)

		const db = admin.firestore()
		const ref = db.collection('jobs').doc(id)
		ref.update({
			publishedAt: admin.firestore.FieldValue.serverTimestamp()
		}).then(fbAnswer => {
			console.log('fbAnswer', fbAnswer)
			res.send({
				data: {
					id,
					isPublished: true
				}
			});
		}).catch((fbError) => {
			console.log('fberror', fbError)
			res.send({
				data: {
					id,
					isPublished: false
				}
			});
		})
	})
}

module.exports = {
	publishJob
}
