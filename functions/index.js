const functions = require('firebase-functions')
const admin = require('firebase-admin')
const { onCreateUser, onDeleteUser } = require('./user')
const { publishJob } = require('./job')
const { doTags } = require('./tags')
// const { touchCompanies } = require('./maintainance')

admin.initializeApp(functions.config().firebase)

exports.createUser = functions.auth.user().onCreate(onCreateUser)

exports.deleteUser = functions.auth.user().onDelete(onDeleteUser)

exports.makeJobHashtags = functions.firestore.document('/jobs/{model}').onWrite((change, context) => {
	return doTags(change, context, 'jobs', 'body')
})

exports.makeCompanyHashtags = functions.firestore.document('/links/{model}').onWrite((change, context) => {
	return doTags(change, context, 'links', 'body')
})

exports.publishJob = functions.https.onRequest(publishJob)

// exports.touchCompanies = functions.https.onRequest(touchCompanies)
