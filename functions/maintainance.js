const functions = require('firebase-functions')
const admin = require('firebase-admin')

const touchCompanies = (req, res) => {
	const db = admin.firestore()
	const linksRef = db.collection('links')

	return linksRef.get()
		.then(snapshot => {

			let promises = []

			snapshot.forEach(doc => {
				console.log(doc.id, '=>', doc.data());
				let linkRef = db.collection('links').doc(doc.id)
				const now = admin.firestore.FieldValue.serverTimestamp()
				const update = linkRef.update({
					updatedAt: now
				})
				promises.push(update)
			})

			console.log('promises', promises)

			// run all promises to update all links
			return Promise.all(promises).then(() => {
				res.send({
					message: 'Touched all links with updatedAt'
				});
			}).catch((fbError) => {
				res.send({
					message: 'Error touching links'
				});
			})
		})
		.catch(err => {
			console.log('Error getting documents', err);
		});

}

module.exports = {
	touchCompanies
}
