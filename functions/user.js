const admin = require('firebase-admin')

const onCreateUser = async (user) => {
	const { uid } = user
  const db = admin.firestore()

	let newSettingsSnap;
	try {
		// create user settings
		newSettingsSnap = await db.collection('userSettings').add({
			user: uid
		})
	} catch(e) {
		throw "Error creating user settings"
	}

	try {
		const newUserRef = db.collection('users').doc(uid)
		await newUserRef.set({
			createdAt: admin.firestore.FieldValue.serverTimestamp(),
			settings: newSettingsSnap.id
		}, { merge: true })
	} catch(e) {
		db.collection('userSettings').doc(newSettingsSnap.id).delete()
		throw "Error creating user and assigning settings"
	}

}

const onDeleteUser = async (user) => {
	const { uid } = user
  const db = admin.firestore()
	let jobsSnapshot;

	if(!uid) {
		throw "Error, need a user.uid param"
	}

	// delete all jobs for this user
	try {
		jobsSnapshot = await db.collection('jobs')
				.where('user', '==', uid)
				.get()
	} catch(e) {
		throw "Error getting user jobs"
	}

	try {
		const jobIds = jobsSnapshot.docs.map(doc => doc.id)
		const promises = jobIds.map(jobId => {
			const jobRef = db.collection('jobs').doc(jobId)
			return jobRef.delete()
		})
		await Promise.all(promises)
	} catch(e) {
		throw "Error deleting user jobs"
	}


	// get user to know its settings id
	try {
		const userRef = db.collection('users').doc(uid)
		let userSnapshot = await userRef.get()

		// delete the associated userSettings
		const user = userSnapshot.data()
		const settings = user.settings
		const settingsRef = db.collection('userSettings').doc(settings)
		await settingsRef.delete()
		await userRef.delete()
	}	catch(e) {
		throw "Error deleting the user or its settings"
	}
}

module.exports = {
	onCreateUser,
	onDeleteUser
}
