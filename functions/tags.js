const functions = require('firebase-functions')
const admin = require('firebase-admin')
const { differenceInMinutes } = require('date-fns')

// find all hashtags in a string
const findHashTags = searchText => {
	// https://regexr.com/46r2p
	var regexp = /(?:\B#)(\w|-?)+\b/g
	let result = searchText.match(regexp);
	if (result) {
		return result.map(item => item.replace('#',''));
	} else {
		return false;
	}
}

// find what changed in a request, on a param
// param is a string, parameter of the model,
// on which we're looking to find what are the changes
// returns the change as a stirng, or '' if no change
const findChangeOnParam = (change, param) => {
	// Grab the current value of what was written to the Realtime Database.
  const bodyAfter = change.after.exists ? change.after.data()[param] : null
	const bodyBefore = change.before.exists ? change.before.data()[param] : null;

	// to force new tags if model is updated
	const updatedAt = bodyAfter && change.after.data()['updatedAt']
	if (updatedAt) {
		const now = new Date()
		const updatedDate = new Date(updatedAt.toDate())
		var diffMinutes = differenceInMinutes(now, updatedDate)

		if (diffMinutes < 1 ) {
			console.log('diffMinute: %s -> forcing changes on %s', diffMinutes, param)
			return bodyAfter
		}
	}

	// return the new body, if there are change
	if (!bodyAfter || bodyAfter === bodyBefore) {
		return ''
	} else {
		return bodyAfter
	}
}

// !! as a convention we use `item` as in `:item`,
// from the Firebase route listener param name
// Listens for new messages added to /messages/:pushId/original and creates an
// uppercase version of the message to /messages/:pushId/uppercase
const doTags = (change, context, modelName, sourceParam) => {
	const db = admin.firestore()
	const itemId = context.params.model
	const paramChange = findChangeOnParam(change, sourceParam)
	if (!paramChange) return false

	// find all hashtags in the body
	const tags = findHashTags(paramChange)

	// write the tags as an array property on the job
	return db.collection(modelName).doc(itemId).update({
		tags: tags
	});
}

module.exports = {
	doTags
}
